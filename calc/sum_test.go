package calc_test

import (
	"testing"

	"gitlab.com/baugoncalves/mentoria/calc"
)

func TestSum(t *testing.T) {
	result := calc.Sum()

	if result != 30 {
		t.Errorf("The calc got a fail")
	}
}
