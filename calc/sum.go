package calc

import (
	"encoding/json"
	"net/http"
)

func SumRoute(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode(Sum())
}

func Sum() int {
	return 10 + 20
}
