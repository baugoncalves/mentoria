package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/baugoncalves/mentoria/calc"
)

func helloWorld(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode("Hello World Developers hi")
}

func workingCI(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode("It worked")
}

func main() {
	var router *mux.Router

	log.Println("Server started on http://localhost:1337")

	router = mux.NewRouter()
	router.HandleFunc("/", helloWorld)
	router.HandleFunc("/workedci", workingCI)
	router.HandleFunc("/sum", calc.SumRoute)

	err := http.ListenAndServe(":1337", router)
	if err != nil {
		log.Println(err.Error())
	}
}
